var http = require('http')
var url = require('url')
var port = process.env.PORT || 1337;
var response;

http.createServer(function(req, res) {
	var url_parts = url.parse(req.url, true);
	var term = url_parts.query.term;
	var langFrom = url_parts.query.from;
	var langTo = url_parts.query.to;	
	response = res;	

	var data = GetCookieData(AskGoogleTranslate, term, langFrom, langTo);	
}).listen(port);

function AskGoogleTranslate(query, recordset, langFrom, langTo){
	var muidb = recordset[0].Muidb;	
	var mtstkn = recordset[0].Mtstkn;
	var options;
	
	if(query.replace(/\s\s+/g, ' ').split(' ').length == 1) {	
		options = {
		  host: 'www.bing.com',
		  port: 80,
		  path: '/translator/api/Dictionary/Lookup?from=' + langFrom + '&to=' + langTo + '&text=' + query,
		  headers: { 
						'Cookie' : "MUIDB=" + muidb + "; mtstkn=" + mtstkn + ";"
					} 
		};

		http.get(options, function(res) {
			 var dataStr = '';
			  res.on('data', function(chunk) {
				dataStr = dataStr + chunk;
			 });
			 res.on('end', function() {
				 response.writeHeader(200, {'content-type': 'text/plain; charset=utf-8'});
				 response.end(dataStr);
			});
		}).on('error', function(e) {
		  console.log("Got error: " + e.message);
		});
    }
    else {
		options = {
			host: 'www.bing.com',
			port: 80,
			path: '/translator/api/Translate/TranslateArray?from=' + langFrom + '&to=' + langTo,
			method: 'POST',
			headers: { 
						'Cookie' : "MUIDB=" + muidb + "; mtstkn=" + mtstkn + ";",
						'Content-Type': 'application/json; charset=UTF-8'
					}
		};
		var req = http.request(options, function(res) {
			var dataStr = '';
			res.on('data', function(chunk) {
				dataStr = dataStr + chunk;
			});
			 res.on('end', function() {
				 response.writeHeader(200, {'content-type': 'text/plain; charset=utf-8'});
				 response.end(dataStr);
			});			
		});
		req.end('[{"text":"'+ query +'"}]');
	}
}

function GetCookieData(returnDataFunc, term, langFrom,langTo){
	var sql = require('mssql');
	
	var config = {
	  user: 'shark',
	  password: 'Perokajla1',
	  server: 'r4og57l19t.database.windows.net',
	  port: 1433,

	  options: {
		encrypt: true
		,database: 'Cro2Eng'
		,useUTC: true
		,connectTimeout: 5000
		,debug:{
			packet: true
			,data: true
			,payload: true
			,token: true
		}
	  }
	};

	sql.connect(config).then(function() {	
		new sql.Request().query('select top 1 [ID],[Muidb],[Mtstkn] from [Cro2Eng].[dbo].[Cookie] order by id desc').then(function(recordset) {
			returnDataFunc(term, recordset, langFrom, langTo);
		}).catch(function(err) {
		});
	}).catch(function(err) {		
	});
}